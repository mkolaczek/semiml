open OUnit2

(*
 * Very simple test case, just to check whether OUnit2 works.
 * It doesn't use compiler code.
 *)
let inc3 x = x + 3
let example_test1 test_ctxt =
  assert_equal 4 (inc3 1);;

(* Basic test which already use compiler's code *)
let approx_func_cost = Analysis.CPS.ApproxFuncCost.expr_cost
let example_test2 test_ctxt =
  let open Lang.CPS.Ast
  in let open Common.Primop.Arith
  in let f1 = Common.Var.create ()
  in let ar1 = Int64.of_int 30
  in let expr_kind = App(Var(f1), [Int(ar1)])
  in let result = approx_func_cost { e_kind = expr_kind; e_tag = Common.Tag.fresh () }
  in assert_equal 2 result

let example_suite =
  "suite">::: [
    "example_test1" >:: example_test1;
    "example_test2" >:: example_test2
  ]

let () = run_test_tt_main example_suite
