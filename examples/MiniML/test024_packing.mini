datatype bool = false | true in
datatype 'a list = nil | cons of { 'a, 'a list } in

let Empty = exception "Empty" in

let rec rev_append ['a] (xs ys : 'a list) : 'a list =
  match xs with
  | nil    => ys
  | cons p => rev_append['a] (#1 p) (cons['a] { #0 p, ys })
  end
in
let rev = fn ['a] (xs : 'a list) => rev_append['a] xs (nil['a]) in

pack type 'a t = { 'a list, 'a list } 
with 
  let empty = fn ['a] =>
    { nil['a], nil['a] }
  in
  let is_empty = fn ['a] (q : 'a t) =>
    match #1 q with
    | nil    => true
    | cons p => false
    end
  in
  let push = fn ['a] (x : 'a) (q : 'a t) =>
    match is_empty['a] q with
    | true  => { nil['a], cons['a] { x, nil['a] } }
    | false => { cons['a] { x, #0 q }, #1 q }
    end
  in
  let pop = fn ['a] (q : 'a t) =>
    match #1 q with
    | nil    => raise Empty : { 'a t, 'a }
    | cons p =>
      match #1 p with
      | nil => { { nil['a], rev['a] (#0 q) }, #0 p }
      | cons l => { { #0 q, #1 p }, #0 p }
      end
    end
  in
  { empty, is_empty, push, pop }
in
  { forall 'a, 'a t
  , forall 'a, 'a t -> bool
  , forall 'a, 'a -> 'a t -> 'a t
  , forall 'a, 'a t -> { 'a t, 'a }
  }
