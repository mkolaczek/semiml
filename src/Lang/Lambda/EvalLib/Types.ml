
module Env = Common.Var.Map
module ValHeap : module type of Common.Heap = Common.Heap
module IntHeap : module type of Common.Heap = Common.Heap

exception Type_error

type val_loc = ValHeap.loc
type int_loc = IntHeap.loc

type answer = unit

type value =
| V_Record    of value list
| V_Int       of int64
| V_Real      of Common.Real.t
| V_Func      of (value -> cont -> state -> answer)
| V_String    of string
| V_ByteArray of int_loc list
| V_Array     of val_loc list
| V_UArray    of int_loc list
| V_Cont      of cont
| V_ValRef    of val_loc
| V_IntRef    of int_loc
| V_Tagged    of value * int
| V_Exn       of value * val_loc

and cont    = value -> state -> answer
and handler = value -> state -> answer
and state   =
  { st_val_heap : value ValHeap.t
  ; st_int_heap : int64 IntHeap.t
  ; st_handler  : handler
  }

module Value = struct
  let as_record value =
    match value with
    | V_Record r -> r
    | _ -> raise Type_error

  let as_int value =
    match value with
    | V_Int n -> n
    | _ -> raise Type_error

  let as_real value =
    match value with
    | V_Real r -> r
    | _ -> raise Type_error

  let as_func value =
    match value with
    | V_Func f -> f
    | _ -> raise Type_error

  let as_cont value =
    match value with
    | V_Cont c -> c
    | _ -> raise Type_error

  let as_val_ref value =
    match value with
    | V_ValRef l -> l
    | _ -> raise Type_error

  let as_exn value =
    match value with
    | V_Exn(v, l) -> (v, l)
    | _ -> raise Type_error

  let is_boxed value =
    match value with
    | V_Record _ | V_Func _ | V_String _ | V_ByteArray _ | V_Array _ 
    | V_UArray _ | V_Cont _ | V_ValRef _ | V_IntRef _ | V_Tagged _ 
    | V_Exn _ -> true
    | V_Int _ -> false
    | V_Real _ -> Common.Real.is_boxed ()

  let phys_eq v1 v2 =
    match v1, v2 with
    | (V_Record _ | V_Func _ | V_String _ | V_ByteArray _ | V_Array _ 
      | V_UArray _ | V_Cont _ | V_Tagged _ | V_Exn _), _ -> v1 == v2
    | V_Int n1, V_Int n2 -> Int64.compare n1 n2 = 0
    | V_Int _, _ -> false
    | V_Real r1, V_Real r2 -> Common.Real.equal r1 r2
    | V_Real _, _ -> false
    | V_ValRef l1, V_ValRef l2 -> ValHeap.equal_loc l1 l2
    | V_ValRef _, _ -> false
    | V_IntRef l1, V_IntRef l2 -> IntHeap.equal_loc l1 l2
    | V_IntRef _, _ -> false

  let print value =
    match value with
    | V_Record _    -> print_endline "RECORD"
    | V_Int n       -> print_endline (Common.SmlConst.sml_string_of_int64 n)
    | V_Real r      -> print_endline (Common.Real.to_sml_string r)
    | V_Func _      -> print_endline "FUNC"
    | V_String s    -> print_endline (Common.SmlConst.sml_string_of_string s)
    | V_ByteArray _ -> print_endline "BYTEARRAY"
    | V_Array _     -> print_endline "ARRAY"
    | V_UArray _    -> print_endline "UARRAY"
    | V_Cont _      -> print_endline "CONT"
    | V_ValRef _    -> print_endline "VAL_REF"
    | V_IntRef _    -> print_endline "INT_REF"
    | V_Tagged _    -> print_endline "TAGGED"
    | V_Exn _       -> print_endline "EXN"
end
