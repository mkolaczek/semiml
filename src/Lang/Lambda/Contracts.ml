open Language

let right_scopes = Common.Contracts.right_scopes
let unique_vars  = Common.Contracts.unique_vars
let unique_tags  = Common.Contracts.unique_tags

let switch_well_formed = Contract.create
  ~description:
    "Every switch expression is well-formed."
  ~languages: [Lambda]
  "contract:switch_well_formed"
