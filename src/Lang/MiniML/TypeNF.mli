
type t =
| Var       of Type.tvar
| Abstract  of TypeView.t list * Type.tcon
| Arrow     of TypeView.t * TypeView.t
| Record    of TypeView.t list
| DataType
    of TypeView.t list
     * Type.tvar list
     * Type.tcon
     * (Type.cname * Type.t option) list
| LocalDataType 
    of (Type.tcon * TypeView.datatype_def) list
     * TypeView.t list
     * Type.tcon
     * TypeView.datatype_def
| ForallVar of TypeView.t TypeView.tvar_binder
| Forall    of TypeView.t TypeView.tcon_binder
| Exists    of TypeView.t TypeView.tcon_binder

val v_nf : TypeEnv.t -> TypeView.t -> t

val nf : TypeEnv.t -> Type.t -> t

val try_as_arrow : TypeEnv.t -> Type.t -> (Type.t * Type.t) option
val try_as_record : TypeEnv.t -> Type.t -> Type.t list option
val try_as_forall_var : 
  TypeEnv.t -> Type.t -> (TypeView.t TypeView.tvar_binder) option
val try_as_forall :
  TypeEnv.t -> Type.t -> (TypeView.t TypeView.tcon_binder) option
val try_as_exists :
  TypeEnv.t -> Type.t -> (TypeView.t TypeView.tcon_binder) option
