
(** Contract [well_typed] states that the program is well-typed *)
val well_typed : Contract.t

(** Contract [unique_vars] states that there are no two different local 
variables represeted by the same value (of type [Common.var]). *)
val unique_vars : Contract.t

(** Contract [unique_tags] states that every tag in the program is unique. *)
val unique_tags : Contract.t
