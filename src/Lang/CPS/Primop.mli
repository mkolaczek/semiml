
type t =
| Arith   of Common.Primop.Arith.t
| FArith  of Common.Primop.FArith.t
| Mem     of Common.Primop.Mem.t
| Repr    of Common.Primop.Repr.t
| Exn     of Common.Primop.Exn.t

val arity       : t -> int
val cps_value_n : t -> int
val cps_cont_n  : t -> int
