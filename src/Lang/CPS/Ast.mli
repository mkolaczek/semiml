type var  = Common.Var.t

type accesspath =
| Offp      of int
| Selp      of int * accesspath

type value =
| Var       of var
| Label     of var
| Int       of int64
| Real      of Common.Real.t
| String    of string

type expr =
  { e_tag  : Common.Tag.t
  ; e_kind : expr_kind
  }
and expr_kind =
| Record    of (value * accesspath) list * var * expr
| Select    of int * value * var * expr
| Offset    of int * value * var * expr
| App       of value * value list
| Fix       of (var * var list * expr) list * expr
| Switch    of value * expr list
| Primop    of Primop.t * value list * var list * expr list

val top_cont : Common.Var.t
