
type tag = Common.Tag.t

type ident =
  { id_tag  : tag
  ; id_name : string
  }

type expr =
  { e_tag  : tag
  ; e_kind : expr_kind
  }
and expr_kind =
| Succ
| Var  of string
| Num  of int
| Abs  of ident list * expr
| App  of expr * expr
| Case of expr * case_fun * case_zero * case_succ

and case_fun  = ident * expr
and case_zero = expr
and case_succ = ident * expr
