
let parser_func lexbuf =
  try YaccParser.main Lexer.token lexbuf with
  | Parsing.Parse_error ->
    Errors.error_pp
      lexbuf.Lexing.lex_start_p
      lexbuf.Lexing.lex_curr_p
      "Syntax error (Unexpected token `%s')."
      (Lexing.lexeme lexbuf);
    raise Errors.Fatal_error
  | Lexer.Invalid_character c ->
    Errors.error_pp
      lexbuf.Lexing.lex_start_p
      lexbuf.Lexing.lex_curr_p
      "Invalid character '%s' (0x%02X)."
      (Char.escaped c) (Char.code c);
    raise Errors.Fatal_error
  | Lexer.Invalid_number x ->
    Errors.error_pp
      lexbuf.Lexing.lex_start_p
      lexbuf.Lexing.lex_curr_p
      "Invalid numeric literal `%s'." x;
    raise Errors.Fatal_error
