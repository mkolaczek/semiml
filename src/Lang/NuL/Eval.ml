
module Env = Common.Var.Map

open Ast

type value =
| Val_Succ
| Val_Num  of int
| Val_Func of (value Lazy.t -> value)

let rec eval env expr =
  match expr.e_kind with
  | Succ  -> Val_Succ
  | Var x -> Lazy.force (Env.find x env)
  | Num n -> Val_Num n
  | Abs(x, body) -> Val_Func (fun arg ->
      eval (Env.add x arg env) body
    )
  | App(e1, e2) ->
    apply ~tag: expr.e_tag (eval env e1) (lazy (eval env e2))
  | Case(expr, case_fun, case_zero, case_succ) ->
    begin match eval env expr with
    | (Val_Succ | Val_Func _) as f ->
      eval (Env.add (fst case_fun) (lazy f) env) (snd case_fun)
    | Val_Num 0 ->
      eval env case_zero
    | Val_Num n ->
      eval (Env.add (fst case_succ) (lazy (Val_Num(n-1))) env) (snd case_succ)
    end

and apply ?tag func arg =
  match func with
  | Val_Succ ->
    begin match Lazy.force arg with
    | Val_Succ | Val_Func _ ->
      Errors.error ?tag: tag
        "Type error: The successor applied to a function.";
      raise Errors.Fatal_error
    | Val_Num n -> Val_Num (n+1)
    end
  | Val_Num _ ->
    Errors.error ?tag: tag
      "Type error: A number applied to an argument.";
    raise Errors.Fatal_error
  | Val_Func func -> func arg

let rec eval_loop result =
  match result with
  | Val_Succ | Val_Func _ ->
    Printf.printf "?> ";
    let x = int_of_string (read_line ()) in
    apply result (lazy (Val_Num x)) |> eval_loop
  | Val_Num n ->
    Printf.printf "%d\n" n

let eval_program expr =
  let first_result = eval Env.empty expr in
  begin match first_result with
  | Val_Succ | Val_Func _ ->
    Printf.printf "Enter the number(s)\n%!"
  | _ -> ()
  end;
  eval_loop first_result
