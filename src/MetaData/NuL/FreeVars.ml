
let table = Hashtbl.create 32

let get tag = 
  Hashtbl.find table tag

let try_get tag =
  try Some(get tag) with
  | Not_found -> None

let set tag fv =
  Hashtbl.replace table tag fv
