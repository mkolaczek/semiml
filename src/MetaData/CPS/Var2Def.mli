open Lang.CPS.Ast

val get     : var -> expr
val try_get : var -> expr option

val set     : var -> expr -> unit
