
let table = Hashtbl.create 1024

let get vr = 
  Hashtbl.find table vr

let try_get vr =
  try Some(get vr) with
  | Not_found -> None

let set vr triviality =
  Hashtbl.replace table vr triviality
