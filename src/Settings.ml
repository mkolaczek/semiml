
let check_contracts = ref false
let internal_log_handle = ref None

let set_check_contracts b =
  check_contracts := b

let get_check_contracts () =
  !check_contracts

let set_internal_log_handle h =
  internal_log_handle := h

let get_internal_log_handle () =
  !internal_log_handle
