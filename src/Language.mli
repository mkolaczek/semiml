type t =
| CPS
| Lambda
| MiniML
| NuL
| RawCPS
| RawMiniML
| RawNuL

val name : t -> string
