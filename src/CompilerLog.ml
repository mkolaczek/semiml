let log level_name fmt =
  match Settings.get_internal_log_handle () with
  | None   -> Printf.ifprintf stderr fmt
  | Some h ->
    Printf.fprintf h ("%s " ^^ fmt ^^ "\n%!") level_name

let debug fmt   = log "DEBUG" fmt
let info fmt    = log "INFO" fmt
let warning fmt = log "WARNING" fmt
let error fmt   = log "ERROR" fmt
