
val calc_on_demand : Lang.CPS.Ast.expr -> Common.Var.Set.t

val analyse : Lang.CPS.Ast.expr -> unit

val contract : Contract.t

val register : unit -> unit
