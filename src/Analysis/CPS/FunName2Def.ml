
open Lang.CPS.Ast


let rec analyse_expr expr = match expr.e_kind with
  | Record(_, _, e)
  | Select(_, _, _, e)
  | Offset(_, _, _, e) -> analyse_expr e
  | App(v, vs) -> ()
  | Fix(decls, e) -> List.iter (
      fun (vname, formals, body) -> begin
        MetaData.CPS.FunName2Def.set vname (body, formals);
        analyse_expr body;
      end
    ) decls
    ; analyse_expr e
  | Switch(_, es)
  | Primop(_, _, _, es) -> List.iter analyse_expr es

let analyse expr =
  let _ = analyse_expr expr in ()

let contract = Contract.create
  ~description: ("Caches a mapping from each function "
              ^ "name (as a variable) to "
              ^ "the corresponding function body and "
              ^ "list of formal parameters")
  ~languages: [Language.CPS]
  "analyse:fun_name2def"

let register () =
  Compiler.register_analysis
    ~lang:      Compiler.Lang_CPS
    ~name:      "CPS:fun_name2def"
    ~require:   [ ]
    ~contracts: [ contract ]
    analyse

