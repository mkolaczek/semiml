
open Lang.CPS.Ast

module VarSet = Common.Var.Set

let fst3 (a,b,c) = a

let vars_from_vals vs = List.fold_left (fun set v -> match v with
    | Var(x) -> Common.Var.Set.add x set
    | _ -> set
  ) Common.Var.Set.empty vs

let rec calc_on_demand expr =
  let calc_many es = List.fold_left (fun s e ->
    Common.Var.Set.union s (calc_on_demand e)
  ) Common.Var.Set.empty es
  in match expr.e_kind with
  | Record(vs, v, e) ->
    calc_on_demand e
    |> Common.Var.Set.union (vars_from_vals @@ List.map fst vs)
    |> Common.Var.Set.remove v
  | Select(_, record, v, e) | Offset(_, record, v, e) ->
    calc_on_demand e
    |> Common.Var.Set.union (vars_from_vals [record])
    |> Common.Var.Set.remove v
  | App(fname, args) ->
    vars_from_vals [fname]
    |> Common.Var.Set.union (vars_from_vals args)
  | Fix(funcs, e) ->
    Common.Var.Set.of_list (List.map fst3 funcs)
    |> Common.Var.Set.diff (calc_on_demand e)
  | Switch(v, es) ->
    calc_many es
    |> Common.Var.Set.union (vars_from_vals [v])
  | Primop(_, args, rets, cs) ->
    let fvs = Common.Var.Set.union (calc_many cs) (vars_from_vals args)
    in Common.Var.Set.diff fvs (Common.Var.Set.of_list rets)

let rec analyse_expr expr =
  let free_vars =
    match expr.e_kind with
    | Record(vs, v, e) ->
      analyse_expr e
      |> Common.Var.Set.union (vars_from_vals @@ List.map fst vs)
      |> Common.Var.Set.remove v
    | Select(_, record, v, e) | Offset(_, record, v, e) ->
      analyse_expr e
      |> Common.Var.Set.union (vars_from_vals [record])
      |> Common.Var.Set.remove v
    | App(fname, args) ->
      vars_from_vals [fname]
      |> Common.Var.Set.union (vars_from_vals args)
    | Fix(funcs, e) ->
      Common.Var.Set.of_list (List.map fst3 funcs)
      |> Common.Var.Set.diff (analyse_expr e)
    | Switch(v, es) ->
      analyse_exprs es
      |> Common.Var.Set.union (vars_from_vals [v])
    | Primop(_, args, rets, cs) ->
      let fvs = Common.Var.Set.union (analyse_exprs cs) (vars_from_vals args)
      in Common.Var.Set.diff fvs (Common.Var.Set.of_list rets)
  in
  MetaData.CPS.FreeVars.set expr.e_tag free_vars;
  free_vars
and analyse_exprs es = List.fold_left (fun s e ->
  Common.Var.Set.union s (analyse_expr e)
  ) Common.Var.Set.empty es

let analyse expr =
  let _ : Common.Var.Set.t = analyse_expr expr in ()

let contract = Contract.create
  ~description: "Free variable analysis"
  ~languages: [Language.CPS]
  "analyse:free_variables"

let register () =
  Compiler.register_analysis
    ~lang:      Compiler.Lang_CPS
    ~name:      "CPS:free_variables"
    ~require:   [ Common.Contracts.unique_tags ]
    (* ~require:   [ Lang.CPS.Contracts.unique_tags ] *)
    ~contracts: [ contract ]
    analyse
