open Lang.Lambda.Ast

let used_tags = Hashtbl.create 32

let rec check_expr expr =
  if Hashtbl.mem used_tags expr.e_tag then false
  else begin
    Hashtbl.add used_tags expr.e_tag ();
    match expr.e_kind with
    | Var _ | Int _ | Real _ | String _ | Prim _ -> true
    | Fn(_, e) | Con(_, e) | Decon(_, e) | ExnValue(e) | ExnName(e) 
    | Select(_,e) | Raise (e) ->
      check_expr e
    | App(e1, e2) | ConExn(e1, e2) | Handle(e1, e2)
      -> check_expr e1 && check_expr e2
    | Fix(defs, e) ->
      List.for_all (fun (_, def) -> check_expr def) defs
      && check_expr e
    | Record(es) ->
      List.for_all check_expr es
    | SwitchExn(e1, l, e2) -> 
      List.for_all (fun (x1, x2) -> check_expr x1 && check_expr x2) l 
      && check_expr e1 && check_expr e2
    | Switch se -> check_expr se.sw_expr 
      && List.for_all (fun (_, e) -> check_expr e) se.sw_con_cases
      && List.for_all (fun (_, e) -> check_expr e) se.sw_constant_cases 
      && match se.sw_default_case with 
         |None -> true
         |Some e -> check_expr e
  end

let check_program expr =
  Hashtbl.reset used_tags;
  check_expr expr

let register () =
  Compiler.register_contract_checker
    ~lang: Compiler.Lang_Lambda
    ~name: "Lambda:unique_tags"
    ~contract: Lang.Lambda.Contracts.unique_tags
    check_program
