open Lang.CPS.Ast

let used_tags = Hashtbl.create 32

let rec check_expr expr =
  if Hashtbl.mem used_tags expr.e_tag then false
  else begin
    Hashtbl.add used_tags expr.e_tag ();
    match expr.e_kind with
    | Record(_, _, e) | Select(_, _, _, e) | Offset(_, _, _, e) ->
      check_expr e
    | App _ -> true
    | Fix(defs, e) ->
      List.for_all (fun (_, _, body) -> check_expr body) defs
      && check_expr e
    | Switch(_, es) | Primop(_, _, _, es) ->
      List.for_all check_expr es
  end

let check_program expr =
  Hashtbl.reset used_tags;
  check_expr expr

let register () =
  Compiler.register_contract_checker
    ~lang: Compiler.Lang_CPS
    ~name: "CPS:unique_tags"
    ~contract: Lang.CPS.Contracts.unique_tags
    check_program
