type t =
| CPS
| Lambda
| MiniML
| NuL
| RawCPS
| RawMiniML
| RawNuL

let name lang =
  match lang with
  | CPS       -> "CPS"
  | Lambda    -> "Lambda"
  | MiniML    -> "MiniML"
  | NuL       -> "NuL"
  | RawCPS    -> "RawCPS"
  | RawMiniML -> "RawMiniML"
  | RawNuL    -> "RawNuL"
