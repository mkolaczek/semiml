
val check : Lang.RawMiniML.Ast.expr -> Lang.MiniML.Ast.expr
val c_type_check : Contract.t

val register : unit -> unit
