
val check_typedef : 
  Env.t ->
  Lang.RawMiniML.Ast.typedef ->
    Lang.MiniML.Type.tvar list * Lang.MiniML.Type.t

val check_datatypes :
  Env.t ->
  Lang.RawMiniML.Ast.datatype_def list ->
    Env.t * Lang.MiniML.Type.datatype_def list

val check : Env.t -> Lang.RawMiniML.Ast.typ -> Lang.MiniML.Type.t
