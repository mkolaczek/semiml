open Lang.CPS.Ast

let fst3 (a,b,c) = a

let unwrap_label value = match value with
  | Var(l) | Label(l) -> l
  | _ -> failwith ("BetaContraction::unwrap_label -- application of non function")

let rec replace_val (val_map, var_map) v = match v with
  | Label(x) ->
    if List.mem_assoc x val_map
    then List.assoc x val_map
    else if List.mem_assoc x var_map
    then Label(List.assoc x var_map)
    else v
  | Var(x) ->
    if List.mem_assoc x val_map
    then List.assoc x val_map
    else if List.mem_assoc x var_map
    then Var(List.assoc x var_map)
    else v
  | other -> other

let add_var_to_args_map (val_map, var_map) x y =
  let var_map' = (x, y) :: var_map
  in (val_map, var_map')

let add_val_to_args_map (val_map, var_map) x y =
  let val_map' = (x, y) :: val_map
  in (val_map', var_map)

let rec replace_vars args_map expr =
  let rec repl_val = replace_val args_map
  and replace_expr e_kind = match e_kind with
    | Record(vs, x, e) ->
      (*
      * What is accesspath precisely and should we do anything with it here?
      *)
      let vs' = List.map (fun (v, ap) -> (repl_val v, ap)) vs
      in let x' = Common.Var.copy x
      in let args_mapping' = add_var_to_args_map args_map x x'
      in let e' = replace_vars args_mapping' e
      in Record(vs', x', e')
    | Select(i, v, x, e) ->
      let v' = repl_val v
      in let x' = Common.Var.copy x
      in let args_mapping' = add_var_to_args_map args_map x x'
      in let e' = replace_vars args_mapping' e
      in Select(i, v', x', e')
    | Offset(i, v, x, e) ->
      let v' = repl_val v
      in let x' = Common.Var.copy x
      in let args_mapping' = add_var_to_args_map args_map x x'
      in let e' = replace_vars args_mapping' e
      in Offset(i, v', x', e')
    | App(f, xs) ->
      let f' = repl_val f
      in let xs' = List.map repl_val xs
      in App(f', xs')
    | Fix(decls, e) ->
      let decls' = List.map (fun (name, formals, body) ->
        let name' = Common.Var.copy name
        in let formals' = List.map Common.Var.copy formals
        in let args_map' = List.fold_left2 add_var_to_args_map args_map formals formals'
        in let args_map'' = add_var_to_args_map args_map' name name'
        in (name', formals', replace_vars args_map'' body)) decls
      in let old_names = List.map fst3 decls
      in let new_names = List.map fst3 decls'
      in let args_map' = List.fold_left2 add_var_to_args_map args_map old_names new_names
      in let e' = replace_vars args_map' e
      in Fix(decls', e')
    | Switch(v, es) ->
      let v' = repl_val v
      and es' = List.map (replace_vars args_map) es
      in Switch(v', es')
    | Primop(p, vs, xs, es) ->
      let vs' = List.map repl_val vs
      in let xs' = List.map Common.Var.copy xs
        (* We don't really need to add all (xs,xs') to all continuations,
         * each continuation needs only one pair from such list *)
      in let args_map' = List.fold_left2 add_var_to_args_map args_map xs xs'
      in let es' = List.map (replace_vars args_map') es
      in Primop(p, vs', xs', es')
  in { e_kind = replace_expr expr.e_kind; e_tag = Common.Tag.fresh () }

let rec increase_inline_count new_count expr =
  let do_recursively () = match expr.e_kind with
    | Record(_, _, e)
    | Select(_, _, _, e)
    | Offset(_, _, _, e) -> increase_inline_count new_count e
    | Switch(_, es)
    | Primop(_, _, _, es) -> List.iter (increase_inline_count new_count) es
    | App(_, _) -> ()
    | Fix(decls, e) -> begin
        List.iter (fun (_,_,b) -> increase_inline_count new_count b) decls;
        increase_inline_count new_count e;
      end
  in begin
    MetaData.CPS.BetaExpandedCount.set expr.e_tag new_count;
    do_recursively ();
  end

let inlined_function expr = match expr.e_kind with
  | App(f, xs) -> 
    let f_name = unwrap_label f
    in let (body, formals) = MetaData.CPS.FunName2Def.get f_name
    in let val_map = List.combine formals xs
    in let previous_count = MetaData.CPS.BetaExpandedCount.get expr.e_tag
    in let new_body =
      body
      |> replace_vars (val_map,[])
    in begin
      increase_inline_count (previous_count + 1) new_body;
      (* Printf.printf "=== INLINED retrieved body\n";
      Printing.Box.print_stdout (Lang.CPS.Pretty.pretty_program body); 
      Printf.printf "=== INLINED to\n";
      Printing.Box.print_stdout (Lang.CPS.Pretty.pretty_program new_body); 
      Printf.printf "=== INLINED end\n\n"; *)
      new_body.e_kind;
    end
  | _ -> failwith "Transform::CPS::BetaExpansion::inlined_function: Non-App node given"
