open Lang.CPS.Ast
open List

let (<|) f x = f x
let (<~) f g x = f(g(x))
let flip f x y = f y x
let rec range a b = if a >= b then [] else a :: range (a+1) b
let fold_with_index f acc l =
  let lWithIndexes = combine l <| range 0 (length l)
  in fold_left f acc lWithIndexes

module VarMap = Common.Var.Map

module VarSet = Common.Var.Set

type fn = (var * var list * expr)

let substitute : var list -> var VarMap.t -> var list =
  fun l mapping ->
    map (fun var ->
          if VarMap.mem var mapping
          then VarMap.find var mapping
          else var) l

let recordElem : var -> value * accesspath =
  fun var -> (Var var, Offp 0)

let mk_expr tag kind =
  { e_tag  = tag
  ; e_kind = kind
  }

let toSet : var list -> VarSet.t =
 fun vars -> fold_left (flip VarSet.add) VarSet.empty vars

let funFreeVars : fn -> VarSet.t =
 fun (name, vars, { e_tag }) ->
    VarSet.add name <|
      VarSet.diff
        (MetaData.CPS.FreeVars.get e_tag)
        (toSet vars)

let funsFreeVars : fn list -> VarSet.t =
  fun fs -> fold_left VarSet.union VarSet.empty (map funFreeVars fs)

let fixFreeVars : fn list -> expr -> var list =
  fun fs {e_tag} ->
    let funs_free_vars = funsFreeVars fs in
    let body_free_vars = MetaData.CPS.FreeVars.get e_tag in
    let free_vars = VarSet.union funs_free_vars body_free_vars in
    VarSet.elements free_vars

(* Returns a mapping from variable to its index in the closure record. *)
let closureDescription : var list -> int VarMap.t =
 fun free_vars ->
   let add = fun m (var, i) -> VarMap.add var i m in
   fold_with_index add VarMap.empty free_vars

let rec transform : expr -> expr =
  fun {e_tag; e_kind} ->
    let mk kind = mk_expr e_tag kind in
    match e_kind with
    | Fix(fs, e) -> transFix e_tag fs e
    | App(Var f, args) -> transApp e_tag f args
    | App(_, _) -> failwith "ClosureConversion: not Var in the first arg of App"
    | Record(e, v, expr) -> mk <| Record(e, v, transform expr)
    | Select(i, r, v, expr) ->  mk <| Select(i, r, v, transform expr)
    | Offset(i, r, v, expr) ->  mk <| Offset(i, r, v, transform expr)
    | Switch(i, e) -> mk <| Switch(i, map transform e)
    | Primop(p, vls, vrs, e) -> mk <| Primop(p, vls, vrs, map transform e)

and transFix : Common.Tag.t -> fn list -> expr -> expr =
  fun tag fs expr ->
    let mk kind = mk_expr tag kind in
    let freeVars = fixFreeVars fs expr in
    let closureDesc = closureDescription freeVars in
    let (defs, funMapping) = transFunctions closureDesc fs in
    let closureVar = Common.Var.create ~name: "closure" () in
    let elems = map recordElem <| substitute freeVars funMapping in
    let offsetFun = fun var i expr ->
      if VarMap.mem var funMapping
        then mk <| Offset(i, Var closureVar, var, expr)
        else expr
    in
    let funClosures = VarMap.fold offsetFun closureDesc (transform expr) in
    let closure = mk <| Record(elems, closureVar, funClosures) in
    mk <| Fix(defs, closure)


and transApp : Common.Tag.t -> var -> value list -> expr =
  fun tag f args ->
    if f == top_cont
    then mk_expr tag <| App(Var f, args)
    else
      let code = Common.Var.create ~name: "code" () in
      let app = mk_expr tag (App(Var code, (Var f)::args)) in
      mk_expr tag (Select(0, (Var f), code, app))

and transFunctions : int VarMap.t -> fn list -> (fn list * var VarMap.t) =
  fun closureDesc fs ->
    let acc (funs, mapping) ((f', args, body), f) =
      ((f', args, body)::funs, VarMap.add f f' mapping)
    in
      fold_left acc ([], VarMap.empty) <| map (transFunction closureDesc) fs

and transFunction : int VarMap.t ->  fn -> (fn * var) =
  fun closureDesc ((name, args, body) as f) ->
    let f' = Common.Var.create ~name: (Common.Var.name name ^ "'") () in
    let closure = Common.Var.create ~name: "closure" () in
    let fv = funFreeVars f in
    let offset = VarMap.find name closureDesc in
    let oClosure = Common.Var.create ~name: "oClosure" () in
    let addFV =
      fun var body ->
        let offset = VarMap.find var closureDesc in
        mk_expr body.e_tag (Select(offset, Var oClosure, var, body))
    in
    let body2 = VarSet.fold addFV fv (transform body) in
    let body3 = mk_expr body.e_tag (Offset(-offset, Var closure, oClosure, body2)) in
    let retfun = (f', closure::args, body3) in
    (retfun, name)

let closure_conversion = Contract.create
  ~description: "Transform program from CPS to closure passing style"
  ~languages: [Language.CPS]
  "transform:closure_conversion"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_CPS
    ~name:   "CPS:closure_conversion"
    ~require:
      [ Analysis.CPS.FreeVars.contract
      ; Common.Contracts.unique_tags
      ]
    ~contracts:
      [ closure_conversion
      ; Lang.CPS.Contracts.closure_conversion
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.CPS.Contracts.right_scopes
      ; Contract.saves_contract Lang.CPS.Contracts.primop_arity
      ; Contract.saves_contract Lang.CPS.Contracts.closure_conversion
      ; Contract.saves_contract Lang.CPS.Contracts.unique_vars
      ]
    transform
