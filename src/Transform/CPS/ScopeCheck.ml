module S = Lang.RawCPS.Ast
module T = Lang.CPS.Ast
module StrMap = Map.Make(String)

let primop_table = Hashtbl.create 32

module PrimopInitializer(G : Common.Primop.Group) : sig
  val init : (G.t -> Lang.CPS.Primop.t) -> unit
end = struct
  let init conv =
    List.iter (fun op ->
      Hashtbl.add primop_table (G.alpha_name op)
        ( conv op
        , G.arity op
        , G.cps_value_n op
        , G.cps_cont_n  op
        )
    ) G.all_values
end

module InitArith  = PrimopInitializer(Common.Primop.Arith)
module InitFArith = PrimopInitializer(Common.Primop.FArith)
module InitMem    = PrimopInitializer(Common.Primop.Mem)
module InitRepr   = PrimopInitializer(Common.Primop.Repr)
module InitExn    = PrimopInitializer(Common.Primop.Exn)

let _ =
  InitArith.init  (fun op -> Lang.CPS.Primop.Arith op);
  InitFArith.init (fun op -> Lang.CPS.Primop.FArith op);
  InitMem.init    (fun op -> Lang.CPS.Primop.Mem op);
  InitRepr.init   (fun op -> Lang.CPS.Primop.Repr op);
  InitExn.init    (fun op -> Lang.CPS.Primop.Exn op)

let extend_env env x =
  let y = Common.Var.create ~name: x () in
  (StrMap.add x y env, y)

let rec check_args env xs =
  match xs with
  | []      -> (env, [])
  | x :: xs ->
    let (env, x)  = extend_env env x in
    let (env, xs) = check_args env xs in
    (env, x :: xs)

let mk_expr tag kind =
  { T.e_tag  = tag
  ; T.e_kind = kind
  }

let rec check_accesspath ap =
  match ap.S.ap_kind with
  | S.Offp n -> T.Offp n
  | S.Selp(n, ap') ->
    if n < 0 then begin
      Errors.error ~tag: ap.S.ap_tag "Selection of field with negative index.";
      raise Errors.Fatal_error
    end;
    T.Selp(n, check_accesspath ap')

let check_value env v =
  let tag = v.S.v_tag in
  match v.S.v_kind with
  | S.Var x ->
    begin match StrMap.find x env with
    | x -> T.Var x
    | exception Not_found ->
      Errors.error ~tag: tag "Unbound variable %s." x;
      raise Errors.Fatal_error
    end
  | S.Label x ->
    begin match StrMap.find x env with
    | x -> T.Label x
    | exception Not_found ->
      Errors.error ~tag: tag "Unbound variable %s." x;
      raise Errors.Fatal_error
    end
  | S.Int n    -> T.Int n
  | S.Real r   -> T.Real r
  | S.String s -> T.String s

let check_value_ap env (v, ap) =
  (check_value env v, check_accesspath ap)

let rec check_expr env e =
  let tag = e.S.e_tag in
  match e.S.e_kind with
  | S.Record(vs, x, e_cont) ->
    let vs = List.map (check_value_ap env) vs in
    let (env, x) = extend_env env x in
    mk_expr tag (T.Record(vs, x, check_expr env e_cont))
  | S.Select(n, v, x, e_cont) ->
    if n < 0 then begin
      Errors.error ~tag: tag "Selection of field with negative index.";
      raise Errors.Fatal_error
    end;
    let v = check_value env v in
    let (env, x) = extend_env env x in
    mk_expr tag (T.Select(n, v, x, check_expr env e_cont))
  | S.Offset(n, v, x, e_cont) ->
    let v = check_value env v in
    let (env, x) = extend_env env x in
    mk_expr tag (T.Offset(n, v, x, check_expr env e_cont))
  | S.App(v, vs) ->
    mk_expr tag (T.App(check_value env v, List.map (check_value env) vs))
  | S.Fix(defs, e_cont) ->
    let (env, tmp_defs) = build_fix_env env defs in
    let defs = List.map (check_fix_def env) tmp_defs in
    mk_expr tag (T.Fix(defs, check_expr env e_cont))
  | S.Switch(v, es) ->
    mk_expr tag (T.Switch(check_value env v, List.map (check_expr env) es))
  | S.Primop(op, args, xs, es) ->
    check_primop tag env op args xs es

and build_fix_env env defs =
  match defs with
  | [] -> (env, [])
  | (x, args, body) :: defs ->
    let (env, x) = extend_env env x in
    let (env, tmp_defs) = build_fix_env env defs in
    (env, (x, args, body) :: tmp_defs)

and check_fix_def env (x, args, body) =
  let (env, args) = check_args env args in
  let body = check_expr env body in
  (x, args, body)

and check_primop tag env op_name args xs es =
  match Hashtbl.find primop_table op_name with
  | (op, arg_n, val_n, cont_n) ->
    if List.length args <> arg_n then begin
      Errors.error ~tag: tag
        "Primitive operation %s expects %d argument(s), \
        but is applied here to %d."
        op_name arg_n (List.length args);
      raise Errors.Fatal_error
    end;
    let args = List.map (check_value env) args in
    if List.length xs <> val_n then begin
      Errors.error ~tag: tag
        "Primitive operation %s produces %d result(s), \
        but %d variable(s) are given here."
        op_name val_n (List.length xs);
      raise Errors.Fatal_error
    end;
    let (env, xs) = check_args env xs in
    if List.length es <> cont_n then begin
      Errors.error ~tag: tag
        "Primitive operation %s expects %d continuation(s), \
        but %d continuation(s) are provied here."
        op_name cont_n (List.length es);
      raise Errors.Fatal_error
    end;
    mk_expr tag (T.Primop(op, args, xs, List.map (check_expr env) es))
  | exception Not_found ->
    Errors.error ~tag: tag
      "Unknown primitive operation %s." op_name;
    raise Errors.Fatal_error

let check source =
  let env = StrMap.singleton (Common.Var.name T.top_cont) T.top_cont in
  check_expr env source

let c_scope_check = Contract.create
  ~description: "Scope checking"
  ~languages: [Language.CPS]
  "transform:scope_check"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_RawCPS
    ~target: Compiler.Lang_CPS
    ~name:   "CPS:scope_check"
    ~contracts:
      [ Lang.CPS.Contracts.right_scopes
      ; Lang.CPS.Contracts.unique_vars
      ; Lang.CPS.Contracts.primop_arity
      ; c_scope_check
      ]
    check
