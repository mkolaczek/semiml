open Lang.NuL.Ast

module VarMap = Common.Var.Map

let rec unique env expr =
  { expr with
    e_kind =
      match expr.e_kind with
      | Succ  -> Succ
      | Var x -> Var (VarMap.find x env)
      | Num n -> Num n
      | Abs(x, body) ->
        let y = Common.Var.copy x in
        Abs(y, unique (VarMap.add x y env) body)
      | App(e1, e2) ->
        App(unique env e1, unique env e2)
      | Case(e, case_fun, case_zero, case_succ) ->
        Case(
          unique env e, 
          unique_case env case_fun,
          unique env case_zero,
          unique_case env case_succ)
  }

and unique_case env (x, body) =
  let y = Common.Var.copy x in
  (y, unique (VarMap.add x y env) body)

let transform expr =
  unique VarMap.empty expr

let c_unique_vars = Contract.create
  ~description: "Restore unique_vars contract"
  ~languages: [Language.NuL]
  "transform:unique_vars"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_NuL
    ~target: Compiler.Lang_NuL
    ~name:   "NuL:unique_vars"
    ~require:
      [ Lang.NuL.Contracts.right_scopes
      ]
    ~contracts:
      [ Lang.NuL.Contracts.right_scopes
      ; Lang.NuL.Contracts.unique_vars
      ; c_unique_vars
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.NuL.Contracts.unique_tags
      ]
    transform
