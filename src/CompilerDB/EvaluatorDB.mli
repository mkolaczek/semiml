
val register_evaluator : Evaluator.t -> unit

val possible_evaluators : 
  'lang Types.language -> Contract.Set.t -> Evaluator.t list
