
val register_pretty_printer : Pretty.t -> unit

val possible_printers : 
  'lang Types.language -> Contract.Set.t -> Pretty.t list
