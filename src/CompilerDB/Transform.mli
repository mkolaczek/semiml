
type ('source, 'target) transformation_data =
  { transform      : ('source, 'target) Types.transformation
  ; name           : string
  ; require        : Contract.t list
  ; contract_rules : Contract.rule list
  }

type t =
| Transform 
  : 'source Types.language 
  * 'target Types.language
  * ('source, 'target) transformation_data -> t
