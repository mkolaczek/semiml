
(** [find_path source is_dest last_cmds] finds the shortest [path] from
[source] to some [dest], such that [is_dest dest = Some v] and retruns 
[path @ last_cmds v] *)
val find_path : 
  GraphTypes.node -> 
  (GraphTypes.node -> 'a option) ->
  ('a -> GraphTypes.path) ->
  GraphTypes.path
