
open Analysis

let analysis_list = ref []

let register_analysis analysis_obj =
  analysis_list := analysis_obj :: !analysis_list

let possible_analyses lang contracts =
  List.filter (fun analysis_obj ->
    match analysis_obj with
    | Analysis(lang2, data) ->
      Types.Language.equal lang lang2 &&
      List.for_all (fun c -> Contract.Set.mem c contracts) data.require
  ) !analysis_list
