
val register_transformation : Transform.t -> unit

val possible_transforms : 
  'lang Types.language -> Contract.Set.t -> Transform.t list
