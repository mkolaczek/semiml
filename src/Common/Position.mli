
type t

val file_name : t -> string

val to_string : t -> string

val of_pp     : Lexing.position -> Lexing.position -> t
val of_lexing : Lexing.position -> t

val parsing_rhs_pos     : int -> t
val parsing_current_pos : unit -> t
