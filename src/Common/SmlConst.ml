
let sml_string_of_int n =
  let str = string_of_int n in
  String.init 
    (String.length str)
    (fun i -> if str.[i] = '-' then '~' else str.[i])

let sml_string_of_int64 n =
  let str = Int64.to_string n in
  String.init 
    (String.length str)
    (fun i -> if str.[i] = '-' then '~' else str.[i])

let sml_string_of_string str =
  let buf = Buffer.create 32 in
  Buffer.add_char buf '"';
  String.iter (fun c ->
    if c = '\007' then Buffer.add_string buf "\\a"
    else if c = '\008' then Buffer.add_string buf "\\b"
    else if c = '\009' then Buffer.add_string buf "\\t"
    else if c = '\010' then Buffer.add_string buf "\\n"
    else if c = '\011' then Buffer.add_string buf "\\v"
    else if c = '\012' then Buffer.add_string buf "\\f"
    else if c = '\013' then Buffer.add_string buf "\\r"
    else if c = '"'  then Buffer.add_string buf "\\\""
    else if c = '\\' then Buffer.add_string buf "\\\\"
    else if c < ' ' then begin
      Buffer.add_string buf "\\^";
      Buffer.add_char buf (Char.chr (Char.code c + 64))
    end else if c <= '~' then Buffer.add_char buf c
    else begin
      Buffer.add_char buf '\\';
      Buffer.add_string buf (string_of_int (Char.code c))
    end
  ) str;
  Buffer.add_char buf '"';
  Buffer.contents buf
