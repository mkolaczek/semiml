
type t =
  { pos_fname      : string
  ; pos_start_line : int
  ; pos_start_col  : int
  ; pos_end_line   : int
  ; pos_end_col    : int
  }

let file_name pos = pos.pos_fname

let to_string pos =
  if pos.pos_start_line <> pos.pos_end_line then
    Printf.sprintf "%s:(%d:%d)-(%d:%d)"
      pos.pos_fname
      pos.pos_start_line
      pos.pos_start_col
      pos.pos_end_line
      pos.pos_end_col
  else if pos.pos_start_col <> pos.pos_end_col then
    Printf.sprintf "%s:%d:%d-%d"
      pos.pos_fname
      pos.pos_start_line
      pos.pos_start_col
      pos.pos_end_col
  else
    Printf.sprintf "%s:%d:%d"
      pos.pos_fname
      pos.pos_start_line
      pos.pos_start_col

let of_pp p1 p2 =
  { pos_fname      = p1.Lexing.pos_fname
  ; pos_start_line = p1.Lexing.pos_lnum
  ; pos_start_col  = 1 + p1.Lexing.pos_cnum - p1.Lexing.pos_bol
  ; pos_end_line   = p2.Lexing.pos_lnum
  ; pos_end_col    = p2.Lexing.pos_cnum - p2.Lexing.pos_bol
  }

let of_lexing p =
  { pos_fname      = p.Lexing.pos_fname
  ; pos_start_line = p.Lexing.pos_lnum
  ; pos_start_col  = 1 + p.Lexing.pos_cnum - p.Lexing.pos_bol
  ; pos_end_line   = p.Lexing.pos_lnum
  ; pos_end_col    = 1 + p.Lexing.pos_cnum - p.Lexing.pos_bol
  }

let parsing_rhs_pos n =
  of_pp (Parsing.rhs_start_pos n) (Parsing.rhs_end_pos n)

let parsing_current_pos () =
  of_pp (Parsing.symbol_start_pos ()) (Parsing.symbol_end_pos ())
